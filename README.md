# TreeSeg

Some R codes and a package (in beta) for changepoint detection in trees (with model selection and dynamic programming). It is based on the Ph.D. work of Solène Thépaut (Chapter 3, see: https://tel.archives-ouvertes.fr/tel-02457109/document)

- Package in TreeSeg
- CodeXP : see AnExample.R 
