#### S. Thépaut codes for Ph.D thesis simulation
#### Functionalised a bit by G. Rigaill
#### A number of TODOs yet to be done...
#### Packages and source
#### scp -r -P2224  TreeSeg/CodeXP grigaill@stat.genopole.cnrs.fr:~/TreeSeg/.
#### ssh grigaill@stat.genopole.cnrs.fr -p2224 -XY
#### scp -r TreeSeg/CodeXP grigaill@192.168.216.233:~/TreeSeg/.
#### ssh grigaill@192.168.216.233 -XY

#### transfer to rei5
rm(list=ls())
library(Rcpp)
library(igraph)
library(rbenchmark)
library(microbenchmark)
library(TreeSeg)
library(tictoc)
library(ggplot2)
library(reshape2)
library(grid)
library(gridExtra)
library(parallel)
library(dplyr)

source("CodeXP/utils_simulate_tree.R") 
n.cores = 35

## TODO: add this script in the package
## TODO: optimize those functions
################################
# 1st experiment               #
################################



## FOR 1ST EXPERIMENT

one.simu.count <- function(i_simu, n, K, numberYear=5){
 l_dataGK    <- simulateBinaryTreeGaussian(n, K) 
 res.Gauss <- getSegmentationGaussian(l_dataGK)
 gaussK <- getNumberCandidate(res.Gauss)

 l_dataG0    <- simulateBinaryTreeGaussian(n, 0) 
 res.Gauss <- getSegmentationGaussian(l_dataG0)
 gauss0 <- getNumberCandidate(res.Gauss)


 l_dataPK   <- simulateBinaryTreePoisson(n, K, numberYear=numberYear, alpha.min=log(2), alpha.max=log(2))
 res.Poiss <- getSegmentationPoisson(l_dataPK)
 poisK <- getNumberCandidate(res.Poiss)

 l_dataP0    <- simulateBinaryTreePoisson(n, 0, numberYear=numberYear, alpha.min=log(2), alpha.max=log(2))
 res.Poiss <- getSegmentationPoisson(l_dataP0)
 pois0 <- getNumberCandidate(res.Poiss)

 dat <- rbind(
  data.frame(method="Gaussian", K=0, NbCand=gauss0, NbDesc=getNumberDescendant(l_dataG0$tree)),
  data.frame(method="Gaussian", K=K, NbCand=gaussK, NbDesc=getNumberDescendant(l_dataGK$tree)),
  data.frame(method="Poisson" , K=0, NbCand=pois0 , NbDesc=getNumberDescendant(l_dataP0$tree)),
  data.frame(method="Poisson" , K=K, NbCand=poisK , NbDesc=getNumberDescendant(l_dataPK$tree))
 )
 dat$i_simu <- i_simu
 return(dat)
}

l_dataG0 <- simulateBinaryTreeGaussian(512, K); unique(getNumberDescendant(l_dataG0$tree))

n.simu  = 100

n = 512 ## Number of nodes that are not leaves in the tree
K = 32   ## Number of breaks in the partition #in thesis was 19 
## system.time(res <- one.simu.count(1, n, K, numberYear=5)) ## about 30 seconds for n=512
all.res.count <- mclapply(1:n.simu, FUN=one.simu.count, n=n, K=K, numberYear=5, mc.cores=n.cores)

mat <- do.call(rbind, all.res.count)

mat.ave <- mat %>% group_by(method, K, NbDesc) %>% summarise(NbCand = mean(NbCand))
mat.ave$Met_And_K <- paste(mat.ave$method, mat.ave$K, sep="-") 


p <- ggplot(mat.ave, aes(x=NbDesc, y=NbCand, col=Met_And_K)) + geom_line() + geom_point() + 
  ylab("Number of Models") + xlab("Number of descendants") + scale_y_continuous(trans = 'log2') + scale_x_continuous(trans = 'log2')
ggsave("NumberOfCandidate.pdf")





