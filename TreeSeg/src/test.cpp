#include <Rcpp.h>
using namespace Rcpp;
using namespace std;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//

//***************************************************************************
// INITIALIZE Candidate with our without cost
//***************************************************************************

// This function initialize a Candidate as a List with 3 attributes. 
// 'coef' is a NumericVector with coefficients for computing the cost of the Candidate
// ''index' is a NumericVector of length the number of children of the root of the Candidate.
// It allows us to know which Candidates were used to construct this one.
// 'tag' is a LogicalVector, its length is the same as index. It indicates whether or not
// there is a break between the root and its children.
// The arguments are two vectors, one numeric, index_, one logical, tag_. 'coef' is initialized as a null vector.
// The output is a List.




// This function initializes a Candidate as a List, with an additionnal information on the 'coef'.
// The inputs a double, coef_, and two vectors, one numeric, index_, and one logical, tag_.
// The first coefficients of 'coef' is set to coef_, the other one are set to zero.
// The output is a List.

// [[Rcpp::export]]
List initializeCostCandidateGeneral(double coef_, NumericVector index_, LogicalVector tag_, int nYear = 2){
  NumericVector coef(nYear+1);
  coef[0] = coef_;
  for(int i = 1; i <nYear+1; i++) coef[i] = 0;
  List out;
  out["coef"] = coef;
  out["index"] = index_;
  out["tag"] = tag_;
  return out;
}



//***************************************************************************
// ADD a data point to a candidate or a List of candidate 
//***************************************************************************

// This function add a node to an existing Candidate (List). It updates 'coef' of the Candidate (List).
// The input are a List, the existing candidate, and two doubles.

// [[Rcpp::export]]
void addPointCandidate(List candidate, double x, double w){
  NumericVector coef = candidate["coef"];
  coef[0] +=  w*x*x;
  coef[1] += -2*w*x;
  coef[2] +=    w;
}
// This function compute the factorial of an integer

//[[Rcpp::export]]
int logfactorial(int n) 
{
  if (n == 0){
    return 0;
  }
  else{
  return log(n) + logfactorial(n-1);
  }
}

// This function does the same as the previous function but for a Poisson distribution
// [[Rcpp::export]]
void addPointCandidatePoisson(List candidate, NumericVector x, double w)
{
  NumericVector coef = candidate["coef"];
  int n = x.size();
  for(int y=1; y < n+1; y++){
     coef[y] += w*x[y-1];
  } 
}
  


// [[Rcpp::export]]
void addPointCandidateMultiGaussian(List candidate, NumericVector x, double w)
{
  NumericVector coef = candidate["coef"];
  int n = x.size();
  for(int y = 2; y < n; y++){
    coef[0] += w*x[y-2]*x[y-2];
    coef[1] += w*1;
    coef[y] += w*x[y-2];
  }
}

// This function add a node to all the Candidates (List) of a list of Candidates (lCandidate).
// It updates 'coef' for all the Candidates of this list.
// The input are a List of List (List of Candidate), and two doubles.

// [[Rcpp::export]]
void addPointListCandidate(List lCandidate, double x, double w){
  int n = lCandidate.size();
  for(int i=0; i < n; i++){
    List candidate = lCandidate[i];
    addPointCandidate(candidate, x, w);
  }
}

// This function does the same as the previous function but for a Poisson distribution

// [[Rcpp::export]]
void addPointListCandidatePoisson(List lCandidate, NumericVector x, double w){
  int n = lCandidate.size();
  for(int i=0; i < n; i++){
    List candidate = lCandidate[i];
    addPointCandidatePoisson(candidate, x,w);
  } 
}

// [[Rcpp::export]]
void addPointListCandidateMultiGaussian(List lCandidate, NumericVector x, double w){
  int n = lCandidate.size();
  for(int i=0; i < n; i++){
    List candidate = lCandidate[i];
    addPointCandidateMultiGaussian(candidate, x, w);
  } 
}
//***************************************************************************
// EVAL and MINIMIZATION of a candidate or list of candidate
//***************************************************************************

// This function return the cost of a given Candidate.
// The inputs are a List, the Candidate, and a double which is supposed to be the mean of the value of
// each node of the last subtree of the Candidate.
// The output is a double.

// [[Rcpp::export]]
double evalCandidate(List candidate, double mu){
  NumericVector coef = candidate["coef"];
  return(coef[0] + mu*(coef[1] + coef[2]*mu));
  
}

// [[Rcpp::export]]
double evalCandidatePoisson(List candidate){
  NumericVector coef = candidate["coef"];
  int nCoef = coef.size();
  double sumObs = sum(coef) - coef[0];
  
  
  double coefSumObs;
  if(sumObs == 0) coefSumObs = 0;
  else coefSumObs = sumObs*log(sumObs);
  
  
  NumericVector logSumObs(nCoef);
  logSumObs[0] = coef[0];
  for (int i = 1; i < nCoef; i++){
    if(coef[i] == 0) logSumObs[i] = 0;
    else logSumObs[i] = - coef[i]*log(coef[i]);
  }
  double out;
  out = sum(logSumObs) + coefSumObs;
  
  return(out);
}

//[[Rcpp::export]]
double evalCandidateMultiGaussian(List candidate){
  NumericVector coef = candidate["coef"];
  int nCoef = coef.size();
  NumericVector meanObs(nCoef-2);
  for(int y = 0; y < nCoef-2; y++){
    meanObs[y] = coef[y+2]/coef[1];
  }
  double out;
  out = coef[0] + sum(meanObs)*(coef[1] -2);
  return(out);
}

// This function compute the mean of the nodes on the last segment of the Candidate and then compute
// the cost of the Candidate using evalCandidate.
// The input is a List (Candidate).
// The output is a double.

// [[Rcpp::export]]
double minCandidate(List candidate){
  NumericVector coef = candidate["coef"];
  
  if(coef[2] > 0){
    double mean = -0.5 * coef[1]/coef[2];
    return(evalCandidate(candidate,mean));
  }
  else {
    return(coef[0]);
  }
}

// [[Rcpp::export]]
double minCandidatePoisson(List candidate){
  NumericVector coef = candidate["coef"];
  if(sum(coef) - coef[0] > 0){
    return(evalCandidatePoisson(candidate));
  }
  else {
    return(coef[0]);
  }
}

//[[Rcpp::export]]
double minCandidateMultiGaussian(List candidate){
  NumericVector coef = candidate["coef"];
  if(coef[1] != 0){
    return(evalCandidateMultiGaussian(candidate));
  }
  else { return(0);}
}



// This function compute the cost of all the Candidates of a list of Candidates. Using the function minCandidate.
// The input is a List of List (List of Candidates)
// The output is a NumericVector which ith coordonate is the cost of the ith Candidate of the List of Candidates.

// [[Rcpp::export]]
NumericVector minListCandidate(List lCandidate, String model){
  int n = lCandidate.size();
  NumericVector out(n);
  if(model == "gaussian"){
  for(int i=0; i < n; i++){
    List candidate = lCandidate[i];
    out[i] = minCandidate(candidate);
  }
  }
  
  if(model == "multiPoisson"){
    
    for(int i=0; i < n; i++){
      List candidate = lCandidate[i];
      out[i] = minCandidatePoisson(candidate);
    }
  }
  
  if (model == "multiGaussian"){
    for(int i = 0; i < n; i++){
      List candidate = lCandidate[i];
      out[i] = minCandidateMultiGaussian(candidate);
    }
  }
  return(out);
}



//***************************************************************************
// Initialize a List of List of Candidate OR Remove Candidate from List 
//***************************************************************************

//[[Rcpp::export]]
List initializeListCandidateForListListCandidate(List lCandidates){
  List lCandAll;
  lCandAll["bestModel"] = 0;
  lCandAll["lCand"] = lCandidates;
  return(lCandAll);
}

// [[Rcpp::export]]
int count_if(LogicalVector x){
  int counter = 0;
  for(int i = 0; i < x.size(); i++) {
    if(x[i] == TRUE) {
      counter++;
    }
  }
  return counter;
}

//[[Rcpp::export]]
List addCandidateToListCandidate(List lCandidates, List newCandidate){
  int nOldLCand = lCandidates.size();
  List newLCandidates(nOldLCand + 1);
  for(int i = 0; i<nOldLCand; i++)
  {
    newLCandidates[i] = lCandidates[i];
  }
  newLCandidates[nOldLCand] = newCandidate;
  return(newLCandidates);
  
}

//[[Rcpp::export]]
List removeCandidatesFromListCandidate(List lCandidates, LogicalVector isRemoved, int nRemoved){
  int nLCand = lCandidates.size();
  List newLCand(nLCand - nRemoved);
  int indexNewL = 0;
  for(int i = 0; i < nLCand; i++)
  {
    if (!isRemoved[i])
    {
      newLCand[indexNewL] = lCandidates[i];
      indexNewL +=1;
    }
  }
  
  return(newLCand);
}





//***************************************************************************
// ADD candidates or list of candidates
//***************************************************************************

// This function compute a List (Candidate) from the Candidates of the two first children of its root.
// The inputs are two List (the two children Candidate), two integers indice0 and indice1, which correspond
// to the index of the Candidate used to create the new Candidate and two booleans tag0 and tag1 which indicate
// if there is a break between the root of the new Candidate and its children.
// The output is a List.

// [[Rcpp::export]]
List sumCandidateGeneralFirst(List candidate1, List candidate2, int indice0, int indice1, bool tag0, bool tag1,int nYear = 2){
  NumericVector coef(nYear+1);
  NumericVector coef1 = candidate1["coef"];
  NumericVector coef2 = candidate2["coef"];
  for(int i = 0; i <nYear+1; i++) coef[i] = coef1[i]+coef2[i];

  NumericVector indice(2);
  LogicalVector tag(2);
  indice[0] = indice0;
  indice[1] = indice1;
  tag[0] = tag0;
  tag[1] = tag1;
  List out;
  out["coef"] = coef;
  out["index"] = indice;
  out["tag"] = tag;
  
  return out;
}



// This function computes a List of new Candidates (List of List) from the List of Candidates of the
// two first children of the root of the new Candidate.
// The inputs are two List of Candidates (List of List)
// It uses the previous function sumCandidateGeneralFirst for each combination of Candidates from the
// two List.
// Since the last Candidate of a list is a Candidate with a break between the root of the Candidate
// and its parent, tag0 is set to 1 if the first Candidate is the last of the first List (same for tag1).
// The output is a List of Candidates (List of List)

// [[Rcpp::export]]
List sumListTwoCandidateFirst(List lCandidate1, List lCandidate2, int nYear = 2){
  bool tag0, tag1;
  int n1 = lCandidate1.size();
  int n2 = lCandidate2.size();
  int n = n1*n2;
  List lOut(n);
  int indexOut = 0;
  for(int i=0; i < n1; i++)
  {
    List candidate1 = lCandidate1[i];
    for(int j=0; j < n2; j++)
    {
      List candidate2 = lCandidate2[j]; 
      tag0 = (i==n1-1);
      tag1 = (j==n2-1);
      lOut[indexOut] = sumCandidateGeneralFirst(candidate1, candidate2, i, j, tag0, tag1, nYear);
      indexOut += 1;
    }
    
  }
  
  return(lOut);
}

// This function is only used if the root of a Candidate has more than 2 children
// It computes a new Candidate from a Candidate created thanks to sumCandidateGeneralFirst or sumCandidateGeneral
// and a Candidate of another child of the root of the Candidate.
// This function add an index corresponding to the index of the Candidate of the new child, and a tag indicating
// if there is a break between the new children and the root of the new Candidate.
// The inputs are two List (Candidates), an integer newIndice, a boolean newTag and an integer nIt.
// nIt gives an indication about which child is the new child (third, fourth, etc...)

// [[Rcpp::export]]
List sumCandidateGeneral(List candidate1, List candidate2, int newIndice, bool newTag, int nIt, int nYear = 2){
  NumericVector coef(nYear+1);
  NumericVector coef1 = candidate1["coef"];
  NumericVector coef2 = candidate2["coef"];
  for(int i = 0; i <nYear+1; i++)
  {
    coef[i] = coef1[i]+coef2[i];
  }

  NumericVector oldIndice = candidate1["index"];
  LogicalVector oldTag = candidate1["tag"];
  NumericVector indice(nIt+1);
  LogicalVector tag(nIt+1);

  for (int j=0;j<nIt;j++)
  {
    indice[j] = oldIndice[j];
    tag[j] = oldTag[j];
  }
  indice[nIt] = newIndice;
  tag[nIt] = newTag;
  List out;
  out["coef"] = coef;
  out["index"] = indice;
  out["tag"] = tag;
  return out;
}

// This function computes the sum of an existing List of  Candidate (List of List) and a List of Candidates
// for a new child.
// The inputs are two List of List (List of Candidates) and an integer nIt such as in the previous function
// // It uses the previous function sumCandidateGeneral for each combination of Candidates from the
// two List.
// The output is a List of List (List of Candidates).

// [[Rcpp::export]]
List sumListTwoCandidate(List lCandidate1, List lCandidate2, int nIt, int nYear = 2){
  bool newTag;
  int n1 = lCandidate1.size();
  int n2 = lCandidate2.size();
  int n = n1*n2;
  List out(n);
  int indexOut = 0;
  for(int i=0; i < n1; i++)
  {
    List candidate1 = lCandidate1[i];
    for(int j=0; j < n2; j++)
    {
      List candidate2 = lCandidate2[j]; 
      newTag = (j==n2-1);
      out[indexOut] = sumCandidateGeneral(candidate1, candidate2, j, newTag, nIt, nYear);
      indexOut += 1;
    }
    
  }
  
  return(out);
}


// This function is used only when the root of a Candidate has only one child.
// It create a new Candidate from the existing Candidate for the child of the root.
// The inputs are a List (Candidate), an integer indice indicating the index of the previous Candidate
// and a boolean tag, indicating if there is a break between the root of the Candidate and its child

// [[Rcpp::export]]
List oneChild(List candidate,int indice_, bool tag_){
  List out;
  out["coef"] = candidate["coef"];
  out["index"] = indice_;
  out["tag"] = tag_;
  return(out);
}

// This function computes a List of Candidate for a new Candidate with only one child.
// The input is a List of List (List of Candidates).
// It uses the previous function OneChildren.
// The output is a List of List (List of Candidates).

// [[Rcpp::export]]
List oneChildList(List lCandidates){
  bool tag;
  int n1 = lCandidates.size();
  List l_out(n1);
  int indexOut = 0;
  for (int i(0); i < n1; i++)
  {
    List candidate = lCandidates[i];
    tag = (i==n1-1);
    l_out[indexOut] = oneChild(candidate, i, tag);
    indexOut +=1;
  }
  return(l_out);
}

// Given the List tof Candidates for all its children, this function computes a List of Candidates (List of List)
// for a node of the tree.
// The input is a List of a List of List (List of List of Candidates).
// If the node only has one child, the function uses the function oneChildList.
// If the nodes has two children, the function uses the function sumListTwoCandidatesFirst.
// If the nodes has more than two children, for each other child, the function uses sumListTwocandidates.
// The output is a List of Candidates.

// [[Rcpp::export]]
List sumListCandidateGeneral(List listLCandidates, int nYear){
  int nChildren(listLCandidates.size());
  List out;
  if(nChildren == 1)
  {
    List newCandidate = listLCandidates[0];
    out = oneChildList(newCandidate);
  }
  else
  {
    List newCandidate1 = listLCandidates[0];
    List newCandidate2 = listLCandidates[1];
    List tempCandidate = sumListTwoCandidateFirst(newCandidate1, newCandidate2, nYear);

    for(int i = 2; i < nChildren; i++)
    {
      newCandidate1 = tempCandidate;
      newCandidate2 = listLCandidates[i];
      tempCandidate = sumListTwoCandidate(newCandidate1, newCandidate2, i, nYear);
    }
    out = tempCandidate;
  }

  return(out);
}

//***************************************************************************
// Sub Main function
//***************************************************************************

//[[Rcpp::export]]
void initializeListLeaves(List vLOut, NumericMatrix dVal, NumericVector dWei, double beta, NumericVector leaf, String model, int nYear = 2){
  int nLeaf = leaf.size();
  for (int i = 0; i < nLeaf; i++)
  {
    int ind = leaf(i) - 1;
    List lCand(2);
    NumericVector indexInit(1);
    LogicalVector tagInit(1);
    List cand0 = initializeCostCandidateGeneral(0,indexInit, tagInit, nYear);
    double min;
    if(model == "gaussian"){
    addPointCandidate(cand0,dVal(ind,0),dWei(ind));
      min = minCandidate(cand0)+beta;
    }
    if(model == "multiPoisson"){
      addPointCandidatePoisson(cand0,dVal(ind,_),dWei(ind));
      min = minCandidatePoisson(cand0) + beta;
    }
    if(model == "multiGaussian"){
      addPointCandidateMultiGaussian(cand0,dVal(ind,_), dWei(ind));
      min = minCandidateMultiGaussian(cand0) + beta;
    }
    
    List cand1 = initializeCostCandidateGeneral(min, indexInit, tagInit, nYear);
    lCand[0] = cand0;
    lCand[1] = cand1;
    List outLCand = initializeListCandidateForListListCandidate(lCand);
    vLOut[ind] = outLCand;
  }
}

//[[Rcpp::export]]
List createChildrenListCandidates(List lTree, List vLOut, int whichNode){
  IntegerVector children = lTree[whichNode];
  int nChildren  = children.size();
  List vLCandidates(nChildren);
  for (int j = 0; j < nChildren; j++)
  {
    int ind3 = children(j)-1;
    List childLCandidatesAll = vLOut[ind3];
    List childLCandidates = childLCandidatesAll["lCand"];
    vLCandidates[j] = childLCandidates;
    
  }
  return(vLCandidates);
}

//[[Rcpp::export]]
List pruningOnNewListCandidate(List tempLCandidates, double beta, String model, int nYear = 2){
  NumericVector minValues = minListCandidate(tempLCandidates, model);
  double minCost = min(minValues);
  int whichMinCost = which_min(minValues);
  List bestCandidate = tempLCandidates[whichMinCost];
  NumericVector index_ = bestCandidate["index"];
  LogicalVector tag_ = bestCandidate["tag"];
  LogicalVector toRemoveCandidate = !(minValues <= minCost + beta);
  int nRemoved = count_if(toRemoveCandidate);
  List finalList = removeCandidatesFromListCandidate(tempLCandidates, toRemoveCandidate, nRemoved);
  
  // Add new candidate with a breaks
  List newCandidate = initializeCostCandidateGeneral(minCost+beta, index_, tag_, nYear);
  List newLCandidates = addCandidateToListCandidate(finalList, newCandidate);
  return(newLCandidates);
}

//***************************************************************************
// BACTRACKING
//***************************************************************************

//This function return the Candidate with the minimum cost among a list of Candidate.
// The input is a List of List (List of Candidates).
// The output is a List (Candidate).

// [[Rcpp::export]]
List getBestCandidate(List lCandidate, String model){
  NumericVector minValues = minListCandidate(lCandidate, model);
  int bestCandidate = which_min(minValues);
  return(lCandidate[bestCandidate]);
}

// This function updates bestModel for several given nodes.
// The inputs are a List of List of List (List of List of Candidates), a NumericVector children 
// indicating for which nodes we update bestModel and a NUmericVector bestModels with the index
// of the best model for each of these nodes.




//[[Rcpp::export]]
void updateListListCandidate(List listLCandidate, NumericVector children, NumericVector bestModels)
{
  int nChildren = children.size();
  for(int i = 0; i < nChildren; i++)
  {
    int ind = children[i] -1;
    List lCandidate = listLCandidate[ind];
    lCandidate["bestModel"] = bestModels[i];
  }
}

// This function updates a NumericMatrix for the first time.
// The inputs are a NumericMatrix mBreaks to be updated, a NumericVector children which indicates for
// which nodes we want to add (or not) a break, a LogicalVector isBreaks indicating ih there is a break
// between the children and the root of the tree, an integer indexBreaks which indicates the row from which
// we update the matrix and an integer whichNode indicating the parent of the children.

// [[Rcpp::export]]
void updateMatrixBreaks(NumericMatrix mBreaks,NumericVector children, LogicalVector isBreak, int indexBreaks, int whichNode)
{
  int nChildren(children.size());
  for (int i = 0; i < nChildren; i++)
  {
    if (isBreak[i] == true)
    {
      mBreaks(indexBreaks,0) = whichNode + 1;
      mBreaks(indexBreaks,1) = children[i];
      indexBreaks +=1;
    }
  }
}

// This function remove the empty rows of a matrix.
// The input are a NumericMatrix mBreaks and an integer indexBreaks indicating the lase empty row of the
// orginal matrix.
// The output is another NumericMatrix, which is the same as mBreaks but without the empty rows.

// [[Rcpp::export]]
NumericMatrix removeZeros(NumericMatrix mBreaks, int indexBreaks)
{
  NumericMatrix breaks(indexBreaks,2);
  for (int i = 0; i < indexBreaks; i++)
  {
    breaks(i,0) = mBreaks(i,0);
    breaks(i,1) = mBreaks(i,1);
  }
  return(breaks);
}

// This function allows us to perform a backtracking algorithm on a tree and the corresponding Candidates
// in order to retrieve the breaks of the best partition (or Candidate) of the original datatree.
// The inputs are a List of List of List (List of List of Candidates), a List Tree and an integer nNoleafs
// indicating until which index the nodes are not leavex of the tree.
// The output is a NumericMatrix indicating the location of the breaks in the best partition of the tree.
// [[Rcpp::export]]
NumericMatrix backtrackGeneral(List listLCandidates, List lTree, int nNoleafs, String model)
{
  // Initialize the matrix with the breaks
  int nTree = lTree.size();
  NumericMatrix mBreaks(nTree, 2);
  
  // Get the models for the roots
  NumericVector children = lTree[0];
  List lCandidateInitAll = listLCandidates[0];
  List lCandidateInit = lCandidateInitAll["lCand"];
  
  // Get the best Candidate and the index and tags of the models used to create it
  List bestCandidate = getBestCandidate(lCandidateInit, model);
  NumericVector best = bestCandidate["index"];
  LogicalVector isBreaks = bestCandidate["tag"];
  
  // update the optimal models for the children of the root
  updateListListCandidate(listLCandidates, children, best);
  
  // update the matrix containing the breaks
  int indexBreaks(0);
  updateMatrixBreaks(mBreaks, children, isBreaks, indexBreaks, 0);
  int fNewBreaks = count_if(isBreaks);
  indexBreaks += fNewBreaks;
  
  // Repeat these steps for all the nodes in the tree
  for (int c = 1; c < nNoleafs; c++)
  {
    NumericVector tempChildren = lTree[c];
    List tempListCandidateAll = listLCandidates[c];
    List tempListCandidate = tempListCandidateAll["lCand"];
    int indBestModel = tempListCandidateAll["bestModel"];
    List tempBestCandidate = tempListCandidate[indBestModel];
    NumericVector tempBest = tempBestCandidate["index"];
    updateListListCandidate(listLCandidates, tempChildren, tempBest);
    LogicalVector tempIsBreaks = tempBestCandidate["tag"];
    updateMatrixBreaks(mBreaks, tempChildren, tempIsBreaks, indexBreaks, c);
    int nNewBreaks = count_if(tempIsBreaks);
    indexBreaks += nNewBreaks;
  }
  
  // Remove the unused lines in the matrix of breaks
  NumericMatrix breaks = removeZeros(mBreaks,indexBreaks);

  return(breaks);
}


//***************************************************************************
// Main function
//***************************************************************************




// [[Rcpp::export]]
List DP_ibp(List lTree, NumericMatrix dVal, NumericVector dWei, double beta, NumericVector noLeaf, NumericVector leaf, String model, int nYear=2)
{
  List allOut;
  int nNodes = lTree.size();
  List vLOut(nNodes);
  int nNoLeaf = noLeaf.size();
  initializeListLeaves(vLOut, dVal, dWei, beta, leaf, model, nYear);
 
  
  for(int i = nNoLeaf ; i > 0; i--)
  {
    int ind2 = noLeaf[i-1] - 1;
    List vLCandidates = createChildrenListCandidates(lTree, vLOut, ind2);
    List tempLCandidates = sumListCandidateGeneral(vLCandidates, nYear);

    // add the root to the created list of Candidate.
    if (model == "gaussian"){
    addPointListCandidate(tempLCandidates,dVal(ind2,0),dWei[ind2]);
    }
    
    if (model == "multiPoisson"){
      addPointListCandidatePoisson(tempLCandidates,dVal(ind2,_), dWei(ind2));
    }
    
    if(model == "multiGaussian"){
      addPointListCandidateMultiGaussian(tempLCandidates, dVal(ind2,_), dWei(ind2));
    }

    // Pruning
    List newLCandidates = pruningOnNewListCandidate(tempLCandidates, beta, model, nYear);

    // Add the List of Candidates to the final List
    List finalLCandidates = initializeListCandidateForListListCandidate(newLCandidates);
    vLOut[ind2] = finalLCandidates;
  }

  NumericMatrix mBreaks = backtrackGeneral(vLOut, lTree, nNoLeaf, model);
  allOut["vLCand"] = vLOut;
  allOut["breaks"] = mBreaks;
  return(allOut);
}


//***************************************************************************
// Main function with model selection
//***************************************************************************

//[[Rcpp::export]]
double newPenalty(int D,int n,double sigma,double c1,double c2, double c3, String model, int p)
{
  double out;
  if(model == "gaussian"){
      out = sigma*sigma*(c1*(log(n)-log(D)-1)+ c2);
  }
  if(model == "multiPoisson"){
    out =c1*(log(n)-log(D)) + c2*p  + c3;
  }
    
  return(out);
}



//[[Rcpp::export]]
List DP_ibp_selection(List lTree, NumericMatrix dVal, NumericVector dWei, double initBeta, NumericVector noLeaf, NumericVector leaf, int maxIt, double sigma, int c1, int c2, int c3, String model, int nYear=2){
  
  List allOut;
  int nTree = lTree.size();
  int oldBeta = 0;
  int iter = 0;
  int beta = initBeta;
  int hatD = 0;
  
  while (beta != oldBeta && iter < maxIt){
    List res = DP_ibp(lTree, dVal, dWei, beta, noLeaf, leaf, model, nYear);
    NumericMatrix mBreaks = res["breaks"];
    hatD = mBreaks.nrow()+1;
    oldBeta = beta;
    beta = newPenalty(hatD, nTree, sigma, c1, c2, c3, model, nYear);
    iter += 1;
    allOut["models"] = res["vLCand"];
    allOut["breaks"] = mBreaks;
  }
  
  NumericVector vBeta(2);
  vBeta[0] = oldBeta;
  vBeta[1] = beta;
  allOut["iter"] = iter;
  allOut["beta"] = vBeta;
  return(allOut);  
}

