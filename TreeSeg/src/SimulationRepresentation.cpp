#include <Rcpp.h>
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//


//***************************************************************************
// SIMULATION
//***************************************************************************

// [[Rcpp::export]]
NumericVector getChildren(List l_tree, NumericVector roots)
{
  int n_roots(roots.size());
  int index_children = 0;
  int n(0);
  for (int i(0);i<n_roots;i++)
  {
    int root = roots[i];
    if (root > 0)
    {
      NumericVector sub1 = l_tree[root -1];
      n += sub1.size();
    }
  }
  if (n==0)
  {
    NumericVector children(1);
    return(children);
  }
  else{
    NumericVector children(n);
    for (int i(0);i<n_roots;i++)
    {
      int root = roots[i];
      if (root >0)
      {
        NumericVector sub = l_tree[root-1];
        int n_children(sub.size());
        for (int j(0);j<n_children;j++)
        {
          if (sub[j] !=0){
          children[index_children] = sub[j];
          index_children +=1;
          }
        }
      }
    }
    return(children);
  }
}

// 
// 
// // [[Rcpp::export]]
// NumericVector getNodesSubtree(List l_tree, int root)
// {
//   int n(l_tree.size());
//   NumericVector temp_nodesSubtree(n);
//   temp_nodesSubtree[0] = root;
//   NumericVector condStop(1);
//   int prev_children(1);
//   int index_nodes(1);
//   bool nonStop = true;
//   while(nonStop)
//   {
//     int nNonZeroChildren(0);
//     NumericVector temp_children(prev_children);
//     for (int i(0);i<prev_children;i++) temp_children[i] = temp_nodesSubtree[index_nodes-prev_children+i];
//     NumericVector new_children = getChildren(l_tree,temp_children);
//     prev_children = new_children.size();
//     for (int i(0);i<prev_children;i++)
//     {
//       if (new_children[i] !=0)
//       {
//         temp_nodesSubtree[index_nodes] = new_children[i];
//         index_nodes +=1;
//         nNonZeroChildren += 1;
//       }
//     }
//     prev_children = nNonZeroChildren;
//     LogicalVector vnonStop = (new_children == 0);
//     nonStop = !is_true(all(vnonStop));
//   }
//   
//   NumericVector nodesSubtree(index_nodes);
//   for (int i(0);i < index_nodes;i++) nodesSubtree[i] = temp_nodesSubtree[i];
//   return(nodesSubtree);
// }


// [[Rcpp::export]]
NumericVector getNodesSubtree(List l_tree, int root)
{
  int n(l_tree.size());
  NumericVector temp_nodesSubtree(n);
  temp_nodesSubtree[0] = root;
  int prev_children =1;
  int index_nodes = 1;
  bool nonStop = true;
  while(nonStop)
  {
    int nNonZeroChildren(0);
    NumericVector temp_children(prev_children);
    for (int i(0);i<prev_children;i++) temp_children[i] = temp_nodesSubtree[index_nodes-prev_children+i];
    NumericVector new_children = getChildren(l_tree,temp_children);
    prev_children = new_children.size();
    for (int i(0);i<prev_children;i++)
    {
      if (new_children[i] !=0)
      {
        temp_nodesSubtree[index_nodes] = new_children[i];
        index_nodes +=1;
        nNonZeroChildren += 1;
      }
    }
    prev_children = nNonZeroChildren;
    LogicalVector vnonStop = (new_children == 0);
    nonStop = !is_true(all(vnonStop));
  }
  
  NumericVector nodesSubtree(index_nodes);
  for (int i(0);i < index_nodes;i++) nodesSubtree[i] = temp_nodesSubtree[i];
  return(nodesSubtree);
}

// [[Rcpp::export]]
List getSubtrees(List l_tree, NumericVector roots, int nBreaks)
{
  
  List out(nBreaks+1);
  int index_out(0);
  NumericVector firstsub = getNodesSubtree(l_tree,1);
  out[index_out] = firstsub;
  NumericVector fsub;
  index_out +=1;
  for (int i(0);i < nBreaks;i++)
  {
    int root(roots[i]);
    NumericVector sub;
    sub = getNodesSubtree(l_tree,root);
    out[index_out] = sub; 
    index_out +=1;
  }

  
  return(out);
}

//***************************************************************************
// REPRESENTATION
//***************************************************************************
// [[Rcpp::export]]
NumericMatrix getAdjacencyMatrix(List l_tree, int n_noleafs, bool bothedges = false)
{
  int n(l_tree.size());
  NumericMatrix mAdjacency(n,n);
  for (int i(0);i<n_noleafs;i++)
  {
    NumericVector children(l_tree[i]);
    int n_children(children.size());
    if (bothedges)
    {
      for (int j(0);j<n_children;j++)
      {
        mAdjacency(i,children[j]-1) = 1;
        mAdjacency(children[j]-1,i) = 1;
      }
    }
    else
    {
      for (int j(0);j<n_children;j++) mAdjacency(i,children[j]-1) = 1;
    }
  }
  return(mAdjacency);
}